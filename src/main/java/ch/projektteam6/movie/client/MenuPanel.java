package ch.projektteam6.movie.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.*;
import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.constants.IconSize;
import org.gwtbootstrap3.client.ui.constants.IconType;

/**
 * Created by patrickduggelin on 28/10/15.
 */
public class MenuPanel extends TabLayoutPanel implements EntryPoint{

    private DataPanel table = new TablePanel();
    private DataPanel graphic= new GraphicPanel();
    private AdminPanel admin = new AdminPanel();

    public MenuPanel(){
        super(5, Style.Unit.EM);
        setAnimationDuration(100);

        add(table, "Table View");
        add(graphic, "Graphic View");
        add(admin, "Admin View");
        selectTab(table);

        setStyleName("gwt-TabLayoutPanel");
        setStyleName("gwt-TabLayoutPanelTab");
        setStyleName("gwt-TabLayoutPanelTab-selected");
        setStyleName("gwt-TabLayoutPanelContent");
    }

    @Override
    public void onModuleLoad() {
        MenuPanel menu = new MenuPanel();

        RootLayoutPanel.get().add(menu);
    }
}