package ch.projektteam6.movie.client;

import ch.projektteam6.movie.shared.Movie;
import java.util.ArrayList;

/**
 * Created by julianiff on 06.11.15.
 */
public class Dummie {


    /**
     * Generates a fake movie ArrayList for testing purposes
     * @param startIndex The starting index
     * @param length The desired length of the ArrayList
     * @return Movie ArrayList
     */
    public static ArrayList<Movie> create(int startIndex, int length){
        ArrayList<Movie> movies = new ArrayList<Movie>();

        for(int i=startIndex; i<startIndex+length; i++){

            String[] countries = {"somecountry", "someothercountry"};
            String[] genres = {"somegenre", "someothergenre"};
            String[] languages = {"somelanguage", "someotherlanguage"};


            Movie movie = new Movie("Movie"+i,1900+i%115,countries,genres,languages, 99+i%250, 1);
            movies.add(movie);

        }

        return movies;
    }
}
