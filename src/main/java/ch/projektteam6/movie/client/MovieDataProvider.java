package ch.projektteam6.movie.client;

import ch.projektteam6.movie.shared.Filter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;
import ch.projektteam6.movie.shared.Movie;

import java.util.ArrayList;

/**
 * Created by patrickduggelin on 01/11/15.
 * Provides the Movie data needed for the table view.
 * Only gets the actually visible movies from the database.
 */
public class MovieDataProvider extends AsyncDataProvider<Movie> {

    private DataBaseServiceAsync dbService;
    private ArrayList<Movie> movies = new ArrayList<Movie>();

    private Filter filter;

    public MovieDataProvider(DataBaseServiceAsync dbService){
        this.dbService=dbService;
    }
    /**
     * Method called when the range of the display (the table in our case) has changed.
     */
    @Override
    protected void onRangeChanged(HasData<Movie> display) {
        refresh((CellTable)display);
    }

    /**
     * Refreshes the table data
     * Should be called when the table needs to be refreshed (when applying a filter)
     */
    protected void refresh(CellTable<Movie> display){
        // Get the new range(visible movie entries) of the table
        final Range range = display.getVisibleRange();
        final ColumnSortList sortList = display.getColumnSortList();
        ColumnSortList.ColumnSortInfo sortInfo = sortList.get(0);
        setMovieCount();
        setMovieData(range, sortInfo);
    }

    /**
     * Makes an Async call to the server to count the movies matching the filter
     */
    private void setMovieCount(){
        AsyncCallback<Integer> callback = new AsyncCallback<Integer>(){
            @Override
            public void onFailure(Throwable throwable) {
                // TODO: Do something with errors.
            }

            @Override
            public void onSuccess(Integer movieCount) {

                updateRowCount(movieCount, true);
            }
        };

        dbService.countMovies(filter, callback);
    }

    /**
     * Makes an Async call to the server to get the movie data matching the filter being in the desired range.
     * Movie Data is sorted according to sortInfo
     */
    private void setMovieData(final Range range, ColumnSortList.ColumnSortInfo sortInfo){
        //Preparing the callback object
        AsyncCallback<ArrayList<Movie>> dataCallback = new AsyncCallback<ArrayList<Movie>>() {
            public void onFailure(Throwable caught) {
                // TODO: Do something with errors.
            }

            @Override
            public void onSuccess(ArrayList<Movie> movies) {
                updateRowData(range.getStart(),movies);
            }
        };

        dbService.readMovies(filter,range.getStart(),range.getLength(),sortInfo.getColumn().getDataStoreName(),sortInfo.isAscending(), dataCallback);
    }

    /**
     * Setter for the filter
     * @param filter
     */
    public void setFilter(Filter filter) {
        this.filter=filter;
    }
}
