package ch.projektteam6.movie.client;

import ch.projektteam6.movie.shared.ComparableAttribute;
import ch.projektteam6.movie.shared.ListAttribute;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import ch.projektteam6.movie.shared.Filter;
import ch.projektteam6.movie.shared.Movie;
import com.google.gwt.view.client.Range;

import java.util.ArrayList;
import java.util.HashMap;


@RemoteServiceRelativePath("DataBaseService")
public interface DataBaseService extends RemoteService {
    // Sample interface method of remote interface
    public void writeMovies(ArrayList<Movie> movies);

    public ArrayList<Movie> readMovies(Filter filter);

    public ArrayList<Movie> readMovies(Filter filter, int start, int length);

    public ArrayList<Movie> readMovies(Filter filter, int start, int length, String sortBy, boolean isAscending);
    public int countMovies(Filter filter);

    public Range getRange(ComparableAttribute c);

    public String[] getAll(ListAttribute l);

    public HashMap<String, Integer> getGenresForCountry(String country, Filter filter);

    public HashMap<String, Integer> getMovieCountForAllCountries(Filter filter);

    /**
     * Utility/Convenience class.
     * Use DataBaseService.App.getInstance() to access static instance of MovieVisServiceAsync
     */
    public static class App {
        private static DataBaseServiceAsync ourInstance = GWT.create(DataBaseService.class);

        public static synchronized DataBaseServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
