package ch.projektteam6.movie.client;

import ch.projektteam6.movie.shared.ComparableAttribute;
import ch.projektteam6.movie.shared.Filter;
import ch.projektteam6.movie.shared.Utils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.extras.slider.client.ui.Range;
import org.gwtbootstrap3.extras.slider.client.ui.RangeSlider;
import org.gwtbootstrap3.extras.slider.client.ui.base.constants.HandleType;


import java.awt.*;

/**
 * Created by julianiff on 06.11.15.
 */
public class FilterPanel extends Composite{

    private VerticalPanel wrapperPanel = new VerticalPanel();
    private HorizontalPanel textInPanel = new HorizontalPanel();
    private HorizontalPanel sliderPanel = new HorizontalPanel();
    private Button submitButton = new Button("Apply Filter");

    private TextBox nameIn = new TextBox();
    private TextBox countriesIn = new TextBox();
    private TextBox languagesIn = new TextBox();
    private TextBox genresIn = new TextBox();
    private RangeSlider yearSlider = new RangeSlider();
    private RangeSlider playTimeSlider = new RangeSlider();

    private DataPanel parentPanel;
    private DataBaseServiceAsync dbService = GWT.create(DataBaseService.class);

    public FilterPanel(DataPanel parent){
        this.parentPanel = parent;
        addWidgets();

        setPlayTimeFullRange();
        setYearFullRange();
        setStyles();
        initWidget(wrapperPanel);
    }


    private void setStyles() {
        nameIn.addStyleName("textInput");
        countriesIn.addStyleName("textInput");
        languagesIn.addStyleName("textInput");
        genresIn.addStyleName("textInput");

        playTimeSlider.setWidth("400px");
        playTimeSlider.addStyleName("slider");
        yearSlider.setWidth("400px");
        yearSlider.addStyleName("slider");

        textInPanel.addStyleName("filterPanel");
        sliderPanel.addStyleName("filterPanel");
        wrapperPanel.addStyleName("filterPanel");
    }

    private void addWidgets() {
        wrapperPanel.add(new Label("Filter Options"));

        textInPanel.add(new Label("Name"));
        textInPanel.add(nameIn);

        textInPanel.add(new Label("Countries"));
        textInPanel.add(countriesIn);
        textInPanel.add(new Label("Languages"));
        textInPanel.add(languagesIn);
        textInPanel.add(new Label("Genres"));
        textInPanel.add(genresIn);

        wrapperPanel.add(textInPanel);

        sliderPanel.add(new Label("Year"));
        sliderPanel.add(yearSlider);
        sliderPanel.add(new Label("Play Time"));
        sliderPanel.add(playTimeSlider);
        submitButton.addClickHandler(new ClickHandler(){

            @Override
            public void onClick(ClickEvent clickEvent) {
                Filter filter = new Filter();
                filter.setName(nameIn.getValue());
                filter.setPlayTimeRange(Utils.toGWTRange(playTimeSlider.getValue()));
                filter.setYearRange(Utils.toGWTRange(yearSlider.getValue()));
                filter.setCountries(Utils.toArray(countriesIn.getValue()));
                filter.setLanguages(Utils.toArray(languagesIn.getValue()));
                filter.setGenres(Utils.toArray(genresIn.getValue()));

                parentPanel.setFilter(filter);
                parentPanel.redrawContent();
            }
        });

        sliderPanel.add(submitButton);

        wrapperPanel.add(sliderPanel);

    }

    private void redrawYearSlider(Range yearFullRange) {
        yearSlider.setMin(yearFullRange.getMinValue());
        yearSlider.setMax(yearFullRange.getMaxValue());
        yearSlider.setValue(yearFullRange);
    }

    public void redrawPlayTimeSlider(Range playTimeFullRange){
        playTimeSlider.setMin(playTimeFullRange.getMinValue());
        playTimeSlider.setMax(playTimeFullRange.getMaxValue());
        playTimeSlider.setValue(playTimeFullRange);
    }

    public void setValuesFromFilter(Filter filter) {
        nameIn.setValue(filter.getName());

        countriesIn.setValue(Utils.toString(filter.getCountries()));
        languagesIn.setValue(Utils.toString(filter.getLanguages()));
        genresIn.setValue(Utils.toString(filter.getGenres()));

        yearSlider.setValue(Utils.toBootstrapRange(filter.getYearRange()));
        playTimeSlider.setValue(Utils.toBootstrapRange(filter.getPlayTimeRange()));
    }

    private void setPlayTimeFullRange() {
        AsyncCallback<com.google.gwt.view.client.Range> callback = new AsyncCallback<com.google.gwt.view.client.Range>() {
            public void onFailure(Throwable caught) {
                // TODO: Do something with errors.
            }

            @Override
            public void onSuccess(com.google.gwt.view.client.Range r) {
                redrawPlayTimeSlider(Utils.toBootstrapRange(r));
            }
        };
        dbService.getRange(ComparableAttribute.PLAYTIME,callback);
    }

    private void setYearFullRange() {
        AsyncCallback<com.google.gwt.view.client.Range> callback = new AsyncCallback<com.google.gwt.view.client.Range>() {
            public void onFailure(Throwable caught) {
                // TODO: Do something with errors.
            }

            @Override
            public void onSuccess(com.google.gwt.view.client.Range r) {
                redrawYearSlider(Utils.toBootstrapRange(r));
            }
        };

        dbService.getRange(ComparableAttribute.YEAR,callback);
    }


}