package ch.projektteam6.movie.client;

import com.google.gwt.core.client.GWT;

import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.*;
import ch.projektteam6.movie.shared.Utils;
import ch.projektteam6.movie.shared.Movie;
import org.gwtbootstrap3.client.ui.Tooltip;
import org.gwtbootstrap3.client.ui.constants.Placement;
import org.gwtbootstrap3.client.ui.gwt.CellTable;

/**
 * Created by patrickduggelin on 28/10/15.
 */
public class TablePanel extends DataPanel {

    private MovieDataProvider dataProvider = new MovieDataProvider(super.getDbService());

    //Main table for the table view
    private CellTable<Movie> table = new CellTable<Movie>();
    private ScrollPanel scroll;

    //Simple Pager for paginantion of the table
    private SimplePager pager;

    /**
     * Constructor
     */
    public TablePanel(){
        init();
        //Initialize the layout panel, then draw the table into it
        initTable();
        redrawContent();
    }

    /**
     * Initialize the Table
     */
    private void initTable() {
        //The header row does not change, so we disable auto refresh
        table.setAutoHeaderRefreshDisabled(true);

        // Set the message to display when the table is empty.
        table.setEmptyTableWidget(new Label("No Movies Found"));

        //Initializig the colums of the table
        initTableColumns();

        //Initializing a simple pager and adding it to the table
        PagerResources r = GWT.create(PagerResources.class);
        pager = new SimplePager(SimplePager.TextLocation.CENTER, r, true, 1000 /* that's the default */,true);
        pager.setDisplay(table);
        pager.setPageSize(100);

        //Adding the movieDataProvider (provides the desired data from the server) to the table
        dataProvider.addDataDisplay(table);

        //Adding the sort handler to the table (actual sorting is implemented on the server)
        ColumnSortEvent.AsyncHandler columnSortHandler = new ColumnSortEvent.AsyncHandler(table);
        table.addColumnSortHandler(columnSortHandler);
        table.getColumnSortList().setLimit(1);

        //Adding the pager and the table to the panel


        //Styling
        FlexTable pagerWrapper = new FlexTable();
        pagerWrapper.setSize("100%","100%");
        pagerWrapper.setWidget(0,0,pager);
        pagerWrapper.getFlexCellFormatter().setVerticalAlignment(0,0, HasVerticalAlignment.ALIGN_MIDDLE);
        pagerWrapper.getFlexCellFormatter().setHorizontalAlignment(0,0, HasHorizontalAlignment.ALIGN_CENTER);
        pager.addStyleName("menu");

        scroll = new ScrollPanel();
        scroll.addStyleName("content");
        scroll.add(table);

        getMainPanel().addNorth(pagerWrapper, 3);
        getMainPanel().add(scroll);
    }

    /**
     * Draw the table
     */
    public void redrawContent() {
        pager.firstPage();
        dataProvider.setFilter(getFilter());
        dataProvider.refresh(table);
    }

    /**
     * Initialize table columns, handles how to interpret the movie data provided by the dataProvider,
     * sets the column headers.
     */
    private void initTableColumns() {
        //Movie Name column
        TextColumn<Movie> nameColumn = new TextColumn<Movie>(){
            @Override
            public String getValue(Movie movie) {
                return movie.getName();
            }
        };
        //column is sortable
        nameColumn.setSortable(true);
        //columns name in the database (for sorting) is "name"
        nameColumn.setDataStoreName("name");
        //column header is "Name"
        table.addColumn(nameColumn, "Name");


        //Movie year column
        TextColumn<Movie> yearColumn = new TextColumn<Movie>(){
            @Override
            public String getValue(Movie movie) {
                if(movie.getYear() != 0) {
                    return Integer.toString(movie.getYear());
                }
                else{
                    return "";
                }
            }
        };
        //column is sortable
        yearColumn.setSortable(true);
        //the table is sorted by this column by default (pushing twice for descending)
        table.getColumnSortList().push(yearColumn);
        table.getColumnSortList().push(yearColumn);
        //columns name in the database (for sorting) is "year"
        yearColumn.setDataStoreName("year");
        //column header is "Year"
        table.addColumn(yearColumn, "Year");


        //
        TextColumn<Movie> countriesColumn = new TextColumn<Movie>(){
            @Override
            public String getValue(Movie movie) {
                if(!movie.getCountries()[0].equals(null)) {
                    return Utils.toString(movie.getCountries());
                }else{
                    return "";
                }
            }
        };
        table.addColumn(countriesColumn, "Countries");

        //
        TextColumn<Movie> languagesColumn = new TextColumn<Movie>(){
            @Override
            public String getValue(Movie movie) {
                if(!movie.getLanguages()[0].equals(null)) {
                    return Utils.toString(movie.getLanguages());
                }else{
                    return "";
                }
            }
        };
        table.addColumn(languagesColumn, "Languages");

        //
        TextColumn<Movie> genresColumn = new TextColumn<Movie>(){
            @Override
            public String getValue(Movie movie) {
                if(!movie.getGenres()[0].equals(null)) {
                    return Utils.toString(movie.getGenres());
                }else{
                    return "";
                }
            }
        };
        table.addColumn(genresColumn, "Genres");

        //
        TextColumn<Movie> playTimeColumn = new TextColumn<Movie>(){
            @Override
            public String getValue(Movie movie) {
                if(movie.getPlayTime() != 0) {
                    return Float.toString(movie.getPlayTime());
                }else{
                    return "";
                }
            }
        };
        playTimeColumn.setSortable(true);
        playTimeColumn.setDataStoreName("playtime");
        table.addColumn(playTimeColumn, "Playtime");
    }

}
