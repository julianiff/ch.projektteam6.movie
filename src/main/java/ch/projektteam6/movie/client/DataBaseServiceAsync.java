package ch.projektteam6.movie.client;

import ch.projektteam6.movie.shared.ComparableAttribute;
import ch.projektteam6.movie.shared.ListAttribute;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import ch.projektteam6.movie.shared.Filter;
import ch.projektteam6.movie.shared.Movie;
import com.google.gwt.view.client.Range;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by julianiff on 06.11.15.
 */
public interface DataBaseServiceAsync {

    public void writeMovies(ArrayList<Movie> movies, AsyncCallback<Void> async);

    public void countMovies(Filter filter, AsyncCallback<Integer> async);

    public void readMovies(Filter filter, int start, int length, AsyncCallback<ArrayList<Movie>> async);

    public void getRange(ComparableAttribute c, AsyncCallback<Range> async);

    public void getAll(ListAttribute l, AsyncCallback<String[]> async);

    public void readMovies(Filter filter, int start, int length, String sortBy, boolean isAscending, AsyncCallback<ArrayList<Movie>> async);

    public void readMovies(Filter filter, AsyncCallback<ArrayList<Movie>> async);

    public void getGenresForCountry(String country, Filter filter,AsyncCallback<HashMap<String, Integer>> async);

    public void getMovieCountForAllCountries(Filter filter, AsyncCallback<HashMap<String, Integer>> async);
}
