package ch.projektteam6.movie.client;

/**
 * Created by julianiff on 06.11.15.
 */
public interface Exportable {

    public void export();
}
