package ch.projektteam6.movie.client;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.SimplePager;

public interface PagerResources extends SimplePager.Resources {
    @Override
    @Source("arrow_white-04.png")
    ImageResource simplePagerFastForward();

    @Override
    @Source("arrow-04.png")
    ImageResource simplePagerFastForwardDisabled();

    @Override
    @Source("arrow_white-05.png")
    ImageResource simplePagerFirstPage();

    @Override
    @Source("arrow-05.png")
    ImageResource simplePagerFirstPageDisabled();

    @Override
    @Source("arrow_white-06.png")
    ImageResource simplePagerLastPage();

    @Override
    @Source("arrow-06.png")
    ImageResource simplePagerLastPageDisabled();

    @Override
    @Source("arrow_white-01.png")
    ImageResource simplePagerNextPage();

    @Override
    @Source("arrow-01.png")
    ImageResource simplePagerNextPageDisabled();

    @Override
    @Source("arrow_white-02.png")
    ImageResource simplePagerPreviousPage();

    @Override
    @Source("arrow-02.png")
    ImageResource simplePagerPreviousPageDisabled();
}