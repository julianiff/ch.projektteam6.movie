package ch.projektteam6.movie.client;

import ch.projektteam6.movie.shared.Filter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.*;


import java.util.ArrayList;
/**
 * Created by julianiff on 06.11.15.
 */
public abstract class DataPanel extends Composite {

    private DockLayoutPanel mainPanel = new DockLayoutPanel(Style.Unit.EM);
    private FilterPanel filterPanel = new FilterPanel(this);

    private DataBaseServiceAsync dbService = GWT.create(DataBaseService.class);
    private Filter filter = new Filter();

    public DockLayoutPanel getMainPanel() {
        return mainPanel;
    }

    public void init(){
        mainPanel.addStyleName("background");
        initFilterPanel();
        initWidget(mainPanel);
    }

    public void initFilterPanel() {
        FlexTable filterWrapper = new FlexTable();
        filterWrapper.setSize("100%","100%");
        filterWrapper.setWidget(0,0,filterPanel);
        filterWrapper.getFlexCellFormatter().setVerticalAlignment(0,0, HasVerticalAlignment.ALIGN_MIDDLE);
        filterWrapper.getFlexCellFormatter().setHorizontalAlignment(0,0, HasHorizontalAlignment.ALIGN_CENTER);
        filterPanel.addStyleName("menu");
        mainPanel.addSouth(filterWrapper,15);
    }

    public DataBaseServiceAsync getDbService() {
        return dbService;
    }

    public Filter getFilter() {
        return filter;
    }
    public void setFilter(Filter filter){
        this.filter = filter;
    }

    public abstract void redrawContent();

}
