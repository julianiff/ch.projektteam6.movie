package ch.projektteam6.movie.client;

import ch.projektteam6.movie.shared.Filter;

import ch.projektteam6.movie.shared.ListAttribute;
import ch.projektteam6.movie.shared.Movie;
import ch.projektteam6.movie.shared.Utils;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.googlecode.gwt.charts.client.*;
import com.googlecode.gwt.charts.client.corechart.PieChart;
import com.googlecode.gwt.charts.client.corechart.PieChartOptions;
import com.googlecode.gwt.charts.client.event.SelectEvent;
import com.googlecode.gwt.charts.client.event.SelectHandler;
import com.googlecode.gwt.charts.client.geochart.GeoChart;
import com.googlecode.gwt.charts.client.geochart.GeoChartColorAxis;
import com.googlecode.gwt.charts.client.geochart.GeoChartOptions;
import com.googlecode.gwt.charts.client.options.PieSliceText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GraphicPanel extends DataPanel{
    private GeoChart geoChart;
    private DataTable dataTable;
    private HashMap<String, Integer> genrePerCountry;
    private HashMap<String, Integer> countriesWithMovieCount;
    boolean firstTime = true;


    public GraphicPanel(){
        init();
        initialize();
    }

    /**
     * Initializes the geo chart
     */
    private void initialize() {
        //boilerplate code from the google api
        ChartLoader chartLoader = new ChartLoader(ChartPackage.GEOCHART);
        chartLoader.loadApi(new Runnable() {
            @Override
            public void run() {
                // Create and attach the chart
                geoChart = new GeoChart();
                //if it is the first time we need a geo chart
                //we draw a geochart with no items first to not
                //have an empty page while loading the actual data
                if (firstTime) {
                    draw();
                    firstTime = false;
                }

                //add a select handler to the geo chart to be able
                //to summon a popup when the user clicks on a country
                geoChart.addSelectHandler(new SelectHandler() {
                    /**
                     * Decide what happens if the user clicks on a country
                     * @param selectEvent used to get the country name
                     */
                    @Override
                    public void onSelect(SelectEvent selectEvent) {
                        //contains the selected row
                        JsArray<Selection> s = geoChart.getSelection();

                        String selectedCountry = getCountryNameFromSelection(s.get(0).getRow());
                        Filter filter = getFilter();

                        //we set the country from the filter to the selected country to get only
                        //the genres which correspond to that country
                        filter.setCountries(new String[]{selectedCountry});

                        //get the genres from the database
                        getGenresForCountry(selectedCountry, filter);
                    }
                });
                //remove the geoChart (the empty one)
                getMainPanel().remove(geoChart);
                //add a new one
                getMainPanel().add(geoChart);

                //get the actual data for the geo chart
                redrawContent();
            }
        });
    }

    /**
     * Fetches the countries from the database and initiates the drawing procedure
     */
    public void redrawContent(){
        countriesWithMovieCount = new HashMap<>();
        final AsyncCallback<HashMap<String,Integer>> countryCallback = new AsyncCallback<HashMap<String,Integer>>() {
            public void onFailure(Throwable caught) {
                // TODO: Do something with errors.
            }

            @Override
            public void onSuccess(HashMap<String,Integer> map) {
                countriesWithMovieCount = map;
                draw();

            }
        };
        //calls the database service on the server
        getDbService().getMovieCountForAllCountries(getFilter(),countryCallback);
    }

    /**
     * Gets the name of the selected country
     * @param row the selected row
     * @return String representation of the country name
     */
    public String getCountryNameFromSelection(int row) {
        return dataTable.getValueString(row, 0);
    }

    /**
     * draws the rows (countries) from the geo chart
     */
    private void draw() {
        // Prepare the data
        dataTable = DataTable.create();
        dataTable.addColumn(ColumnType.STRING, "Country");
        dataTable.addColumn(ColumnType.NUMBER, "Number of movies");
        //if it is the first time, we fill it with dummy values
        if (firstTime) {
            dataTable.addRows(1);
            dataTable.setValue(0,0,"not_loaded");
            dataTable.setValue(0,1,0);
        } else {
            dataTable.addRows(countriesWithMovieCount.size());

            //iterate through the hashmap and fill the rows
            int i = 0;
            for (Map.Entry<String, Integer> entry : countriesWithMovieCount.entrySet()) {
                String country = entry.getKey();
                Integer counter = entry.getValue();

                dataTable.setValue(i, 0, country);
                dataTable.setValue(i, 1, counter);
                i++;
            }
        }
        // Set options
        GeoChartOptions options = GeoChartOptions.create();
        GeoChartColorAxis geoChartColorAxis = GeoChartColorAxis.create();
        geoChartColorAxis.setColors("pink", "#FF4BAD", "deeppink");
        options.setColorAxis(geoChartColorAxis);
        options.setDatalessRegionColor("gray");

        // Draw the chart
        geoChart.draw(dataTable, options);
    }

    /**
     * Get the genres for each country
     * @param country the country the user clicked on the map
     * @param filter the filter to which the result should correspond
     */
    private void getGenresForCountry(final String country, Filter filter){
        //Preparing the callback object
        genrePerCountry = new HashMap<String, Integer>();

        AsyncCallback<HashMap<String,Integer>> callback = new AsyncCallback<HashMap<String,Integer>>() {
            public void onFailure(Throwable caught) {
                // TODO: Do something with errors.
            }

            @Override
            public void onSuccess(HashMap<String, Integer> gpc) {
                genrePerCountry=gpc;
                final PieChartPopup popup = new PieChartPopup(country);
                popup.setPopupPositionAndShow(new PopupPanel.PositionCallback() {
                    public void setPosition(int offsetWidth, int offsetHeight) {
                        int left = (Window.getClientWidth() - offsetWidth) / 3;
                        int top = (Window.getClientHeight() - offsetHeight) / 3;
                        popup.setPopupPosition(left, top);
                    }
                });
            }
        };
        getDbService().getGenresForCountry(country, filter, callback);

    }

    //private class for the pie chart popup
    private class PieChartPopup extends PopupPanel {
        public PieChartPopup(String country) {
            // PopupPanel's constructor takes 'auto-hide' as its boolean parameter.
            // If this is set, the panel closes itself automatically when the user
            // clicks outside of it.
            super(true);

            //draw the pie chart inside a simple panel and add it to the popup panel
            SimplePanel simp = new SimplePanel();
            GenrePieChart pChart = new GenrePieChart(country, simp, genrePerCountry);
            setWidget(simp);
        }
    }

    //private class for the pie chart
    private class GenrePieChart{

        private PieChart pieChart;
        private HashMap<String, Integer> genrePerCountry;

        /**
         * constructor for the pie chart
         * @param country the selected country
         * @param panel the panel to which we add the pie chart
         * @param genrePerCountry the hashmap which contains the genres
         */
        public GenrePieChart(String country, Panel panel, HashMap<String, Integer> genrePerCountry) {
            this.genrePerCountry = genrePerCountry;
            initializePieChart(country, panel);
        }


        /**
         * Initialize the pie chart
         * @param country selected country
         * @param panel panel to which we add the pie chart
         */
        private void initializePieChart(final String country, final Panel panel) {
            //boilerplate code from the api
            ChartLoader chartLoader = new ChartLoader(ChartPackage.CORECHART);
            chartLoader.loadApi(new Runnable() {

                @Override
                public void run() {
                    // Create and attach the chart
                    pieChart = new PieChart();
                    drawPieChart(country);
                    panel.add(pieChart);
                }
            });
        }

        /**
         * draws the pie chart
         * @param country selected country
         */
        private void drawPieChart(String country) {
            // Prepare the data
            DataTable dataTable = DataTable.create();
            dataTable.addColumn(ColumnType.STRING, "Genre");
            dataTable.addColumn(ColumnType.NUMBER, "Amount of movies with genre in selected land");
            dataTable.addRows(genrePerCountry.size());

            //iterate through the hashmap and fill the rows
            int i = 0;
            for (Map.Entry<String, Integer> entry : genrePerCountry.entrySet()) {
                String genre = entry.getKey();
                Integer counter = entry.getValue();

                dataTable.setValue(i, 0, genre);
                dataTable.setValue(i, 1, counter);
                i++;
            }

            // Set options
            PieChartOptions options = PieChartOptions.create();

            //good for internet explorer (at least I think so)
            options.setForceIFrame(true);
            //don't display any label on the pie chart
            options.setPieSliceText(PieSliceText.NONE);
            //donut chart
            options.setPieHole(0.4);

            //genres with a percentage less than 1% are combined to others
            options.setPieResidueSliceColor("#000000");
            options.setPieResidueSliceLabel("Others");
            options.setSliceVisibilityThreshold(0.01);

            //title of the pie chart
            options.setTitle("Genre distribution among the movies from " + country);

            // Draw the chart
            pieChart.draw(dataTable, options);
        }
    }
}

