package ch.projektteam6.movie.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;

public class AdminPanel extends Composite {
    /**
     * AdminPanel constructor
     */
    public AdminPanel(){
        //Create a form panel for the upload since it has to be wrapped in a form panel
        //if we want to submit it to a server
        final FormPanel form = new FormPanel();

        //to structure the form panel and the other panels (i.e. upload status)
        final VerticalPanel panel = new VerticalPanel();

        //sets the action of the form to the FileUploadServlet on the server
        //which means that once someone presses the submit button, the
        //FileUploadServlet gets triggered
        form.setAction(GWT.getModuleBaseURL()+"fileupload");

        // Because we're going to add a FileUploadServlet widget, we'll need to set the
        // form to use the POST method. Therefore we need a doPost method on the FileUploadServlet
        form.setEncoding(FormPanel.ENCODING_MULTIPART);
        form.setMethod(FormPanel.METHOD_POST);

        form.setWidget(panel);

        // Create a FileUpload widget.
        final FileUpload upload = new FileUpload();
        //if supported by the browser: allows only .tsv files to be selected
        upload.getElement().setAttribute("accept", ".tsv");
        upload.setName("uploadFormElement");
        panel.add(upload);

        //image to fake processing
        final Image loading = new Image("/images/loading.gif");

        //Submit button which triggers the forms setAction()
        final Button submit = new Button("Submit", new ClickHandler() {
            public void onClick(ClickEvent event) {
                form.submit();
            }
        });

        panel.add(submit);

        // Add an event handler to the form.
        form.addSubmitHandler(new FormPanel.SubmitHandler() {
            public void onSubmit(FormPanel.SubmitEvent event) {
                // This event is fired just before the form is submitted.
                // We remove the submit button to not allow multiple uploads at a time
                panel.remove(submit);
                // We add our fancy spinning wheel
                panel.add(loading);
            }
        });
        form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
            public void onSubmitComplete(FormPanel.SubmitCompleteEvent event) {
                // When the form submission is successfully completed, this event is
                // fired.
                // We remove our spinning wheel and add the submit button
                panel.remove(loading);
                panel.add(submit);
                // displays the message from the server
                Window.alert(event.getResults());
            }
        });
        initWidget(form);
    }
}
