package ch.projektteam6.movie.shared;


import org.gwtbootstrap3.extras.slider.client.ui.Range;

/**
 * Created by patrickduggelin on 03/11/15.
 */
public class Utils {
    public static final String DEFAULT_SEPERATOR = ", ";

    public static String toString(String[] arr, String separator, String pre, String post){
        if (arr.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();

        sb.append(pre);
        for (String element : arr) {
            sb.append(element).append(separator);
        }

        sb.delete(sb.length()-separator.length(),sb.length());
        sb.append(post);
        return sb.toString();
    }
    /**
     * Convert an array of Strings to a single String with a given seperator
     * @param arr An array of Strings to convert to a single String
     * @param separator A String that is placed between every element of arr
     * @return A single String
     */
    public static String toString(String[] arr, String separator){
        return toString(arr,separator,"","");
    }

    public static String toString(String[] arr){
        return toString(arr,DEFAULT_SEPERATOR);
    }

    public static String[] toArray(String string){
        return toArray(string,DEFAULT_SEPERATOR);
    }

    public static String[] toArray(String string, String separator){
        return string.split(separator);
    }

    public static Range toBootstrapRange(com.google.gwt.view.client.Range r){
        return new Range(r.getStart(),r.getStart()+r.getLength());
    }


    public static com.google.gwt.view.client.Range toGWTRange(Range r) {
        return new com.google.gwt.view.client.Range((int)r.getMinValue(),(int)(r.getMaxValue()-r.getMinValue()));
    }
}
