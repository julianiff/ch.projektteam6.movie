package ch.projektteam6.movie.shared;

/**
 * Created by patrickduggelin on 06/11/15.
 */
public enum ComparableAttribute {
    PLAYTIME,
    YEAR
}
