package ch.projektteam6.movie.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.ArrayList;

/**
 * Created by patrickduggelin on 28/10/15.
 */

public class Movie implements IsSerializable {

    private Long id;
    private String name;
    private int year;
    private String[] countries;
    private String[] genres;
    private String[] languages;
    private int playTime;
    private int wikipediaID;


    public Movie(String name, int year, String[] countries, String[] genres, String[] languages, int playTime, int wikipediaID) {
        this.name = name;
        this.year = year;
        this.countries = countries;
        this.genres = genres;
        this.languages = languages;
        this.playTime = playTime;
        this.wikipediaID = wikipediaID;
    }

    public Movie() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String[] getCountries() {
        return countries;
    }

    public void setCountries(String[] countries) {
        this.countries = countries;
    }

    public String[] getGenres() {
        return genres;
    }

    public void setGenres(String[] genres) {
        this.genres = genres;
    }

    public String[] getLanguages() {
        return languages;
    }

    public void setLanguages(String[] languages) {
        this.languages = languages;
    }

    public int getPlayTime() {
        return playTime;
    }

    public void setPlayTime(int playTime) {
        this.playTime = playTime;
    }

    public void setWikipediaID(int wikipediaID) {
        this.wikipediaID = wikipediaID;
    }

    public int getWikipediaID(){
        return wikipediaID;
    }
}
