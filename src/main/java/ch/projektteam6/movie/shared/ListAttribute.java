package ch.projektteam6.movie.shared;

/**
 * Created by patrickduggelin on 10/11/15.
 */
public enum ListAttribute {
    COUNTRIES,
    LANGUAGES,
    GENRES
}
