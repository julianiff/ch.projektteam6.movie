package ch.projektteam6.movie.shared;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.Range;

import java.util.ArrayList;


/**
 * Created by patrickduggelin on 28/10/15.
 */
public class Filter implements IsSerializable {
    private String name;

    private Range yearRange;
    private Range playTimeRange;

    private String[] countries;
    private String[] languages;
    private String[] genres;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getCountries() {
        return countries;
    }

    public void setCountries(String[] countries) {
        this.countries = countries;
    }

    public String[] getLanguages() {
        return languages;
    }

    public void setLanguages(String[] languages) {
        this.languages = languages;
    }

    public String[] getGenres() {
        return genres;
    }

    public void setGenres(String[] genres) {
        this.genres = genres;
    }

    public Range getYearRange() {
        return yearRange;
    }

    public void setYearRange(Range yearRange) {
        this.yearRange = yearRange;
    }

    public Range getPlayTimeRange() {
        return playTimeRange;
    }

    public void setPlayTimeRange(Range playTimeRange) {
        this.playTimeRange = playTimeRange;
    }

    public Filter(){

    }
}
