package ch.projektteam6.movie.server;

import ch.projektteam6.movie.shared.*;

import com.google.appengine.api.utils.SystemProperty;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import ch.projektteam6.movie.client.DataBaseService;
import com.google.gwt.view.client.Range;


/**
 * Created by julianiff on 06.11.15.
 */
public class DataBaseServiceImpl extends RemoteServiceServlet implements DataBaseService {

    private String url;
    private String user = "glinz";
    private String password = "123lolSamichlaus";
    private String dbName = "se_project";

    public DataBaseServiceImpl(){}
    /**
     * Gets the connection for all of our commands
     *
     * @return
     * @throws Exception
     */
    private Connection getConnection() throws Exception {
        String testDriver = "com.mysql.jdbc.Driver";
        String prodDriver = "com.mysql.jdbc.GoogleDriver";
        Connection conn = null;


        if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Production) {
            try {
                Class.forName(prodDriver);
            }catch(Exception e){
                e.printStackTrace();
            }
            url = "jdbc:google:mysql://software-engineering-1118:cloudsql/se_project?user=root";
            conn = DriverManager.getConnection(url);

        } else {
            try {
                Class.forName(testDriver);
            }catch(Exception e){
                e.printStackTrace();
            }
            url = "jdbc:mysql://team6.lol:3306/";
            conn = DriverManager.getConnection(url+dbName,user,password);
        }
        try{

        }catch(Exception e){
            e.printStackTrace();
        }

        return conn;
    }

    /**
     * Writng all the Movies in the ArrayList to the database
     * @param movies
     */
    public void writeMovies(ArrayList<Movie> movies){
        Connection connection = null;

        try {
            connection = getConnection();

            System.out.println("Starting to write movies to DB:");
            for(Movie movie : movies) {
                insertMovie(connection, movie);
                if(movie.getCountries() != null)insertCountries(connection, movie);
                if(movie.getLanguages() != null)insertLanguages(connection, movie);
                if(movie.getGenres() != null)insertGenres(connection,movie);
            }
            connection.close();
        } catch (Exception sqle) {
            sqle.printStackTrace();
            System.out.println(sqle);
        }
    }

    /**
     *
     * @param connection
     * @param movie
     * @throws SQLException
     */
    private void insertCountries(Connection connection, Movie movie) throws SQLException {
        for(String country : movie.getCountries()) {
            if(country != null) {
                PreparedStatement pstm = null;
                pstm = connection.prepareStatement("INSERT INTO countries (movies_wikipedia_id,name) VALUES(?,?)");
                pstm.setInt(1, movie.getWikipediaID());
                pstm.setString(2, country);

                System.out.println(pstm.toString());

                pstm.execute();
                pstm.close();
            }
        }
    }

    private void insertGenres(Connection connection, Movie movie) throws SQLException {
        for(String genre : movie.getGenres()) {
            if(genre != null) {
                PreparedStatement pstm = null;
                pstm = connection.prepareStatement("INSERT INTO genres (movies_wikipedia_id,name) VALUES(?,?)");
                pstm.setInt(1, movie.getWikipediaID());
                pstm.setString(2, genre);

                pstm.execute();
                pstm.close();
            }
        }
    }

    /**
     *
     * @param connection
     * @param movie
     * @throws SQLException
     */
    private void insertLanguages(Connection connection, Movie movie) throws SQLException {
        for(String language : movie.getLanguages()) {
            if(language != null) {
                PreparedStatement pstm = null;
                pstm = connection.prepareStatement("INSERT INTO languages (movies_wikipedia_id,name) VALUES(?,?)");
                pstm.setInt(1, movie.getWikipediaID());
                pstm.setString(2, language);

                pstm.execute();
                pstm.close();
            }
        }
    }

    /**
     *
     * @param connection
     * @param movie
     * @throws SQLException
     */
    private void insertMovie(Connection connection, Movie movie) throws SQLException {
        PreparedStatement pstm = null;
        pstm = connection.prepareStatement("INSERT INTO movies (name, year, wikipedia_id, playtime) VALUES(?,?,?,?)");
        pstm.setString(1, movie.getName());
        pstm.setInt(2, movie.getYear());
        pstm.setInt(3, movie.getWikipediaID());
        pstm.setInt(4, movie.getPlayTime());

        pstm.execute();
        pstm.close();
    }

    @Override
    public ArrayList<Movie> readMovies(Filter filter) {
        return readMovies(filter,-1,-1);
    }
    /**
     * Return an ArrayList of Movies matching the filter
     * @param filter
     * @return
     */
    public ArrayList<Movie> readMovies(Filter filter, int start, int length, String sortBy, boolean isAscending){

        ArrayList<Movie> resultMovies = new ArrayList<Movie>();
        Connection connection = null;
        ResultSet resultSet;

        long startTime = System.currentTimeMillis();

        try {
            connection = getConnection();
            PreparedStatement pstm = null;

            StringBuilder sb = new StringBuilder();

            sb.append(getSelectStatement());
            sb.append(getFilterStatement(filter));
            sb.append(getSortStatement(sortBy, isAscending));
            sb.append(getRangeStatement(start, length));

            System.out.println(sb.toString());

            pstm = connection.prepareStatement(sb.toString());
            resultSet = pstm.executeQuery();

            while(resultSet.next()){
                Movie movie = new Movie();

                movie.setWikipediaID(resultSet.getInt("wikipedia_id"));
                movie.setName(resultSet.getString("name"));
                movie.setYear(resultSet.getInt("year"));
                movie.setPlayTime(resultSet.getInt("playtime"));

                movie.setLanguages(getLanguages(connection, resultSet.getInt("wikipedia_id")));
                movie.setGenres(getGenres(connection, resultSet.getInt("wikipedia_id")));
                movie.setCountries(getCountries(connection, resultSet.getInt("wikipedia_id")));
                resultMovies.add(movie);
            }
            pstm.close();
            connection.close();
        } catch (Exception sqle) {
            sqle.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Time for getting all movie data: "+(endTime-startTime)+"ms");
        return resultMovies;
    }

    private String getRangeStatement(int start, int length) {
        if(start < 0 || length < 0){
            return "";
        }else{
            return " LIMIT " + length + " OFFSET " + start;
        }

    }

    private String getSortStatement(String sortBy, boolean isAscending) {
        if(sortBy == null) {
            System.out.println("is null");
            return "";
        }else{
            String s = isAscending ? "ASC" : "DESC";
            return " ORDER BY "+sortBy+" "+s;
        }
    }

    @Override
    public ArrayList<Movie> readMovies(Filter filter, int start, int length) {
        return readMovies(filter,start,length,null,true);
    }

    /**
     *
     * @param conn
     * @param wikipedia_id
     * @return
     */
    private String[] getCountries(Connection conn,int wikipedia_id) {
        ArrayList<String> resultCountries = new ArrayList<String>();
        ResultSet resultSet;
        try {
            PreparedStatement pstm = null;

            pstm = conn.prepareStatement("SELECT name FROM countries WHERE movies_wikipedia_id ="+wikipedia_id);
            resultSet = pstm.executeQuery();

            while(resultSet.next()){
                resultCountries.add(resultSet.getString("name"));
            }
        } catch (Exception sqle) {
            sqle.printStackTrace();
        }
        return resultCountries.toArray(new String[resultCountries.size()]);
    }

    /**
     *
     * @param conn
     * @param wikipedia_id
     * @return
     */
    private String[] getGenres(Connection conn, int wikipedia_id) {
        ArrayList<String> resultGenres = new ArrayList<String>();
        ResultSet resultSet;
        try {
            PreparedStatement pstm = null;

            pstm = conn.prepareStatement("SELECT name FROM genres WHERE movies_wikipedia_id ="+wikipedia_id);
            resultSet = pstm.executeQuery();

            while(resultSet.next()){
                resultGenres.add(resultSet.getString("name"));
            }
        } catch (Exception sqle) {
            sqle.printStackTrace();
        }
        return resultGenres.toArray(new String[resultGenres.size()]);
    }

    /**
     *
     * @param conn
     * @param wikipedia_id
     * @return
     */
    private String[] getLanguages(Connection conn, int wikipedia_id) {
        ArrayList<String> resultLanguages = new ArrayList<String>();
        ResultSet resultSet;
        try {
            PreparedStatement pstm = null;

            pstm = conn.prepareStatement("SELECT name FROM languages WHERE movies_wikipedia_id ="+wikipedia_id);
            resultSet = pstm.executeQuery();

            while(resultSet.next()){
                resultLanguages.add(resultSet.getString("name"));
            }
        } catch (Exception sqle) {
            sqle.printStackTrace();
        }
        return resultLanguages.toArray(new String[resultLanguages.size()]);
    }

    /**
     *
     * @return
     */
    private String getSelectStatement() {
        return  "SELECT DISTINCT m.name, m.playtime, m.year, m.wikipedia_id FROM movies AS m ";
    }

    /**
     *
     * @param filter
     * @return
     */
    private String getFilterStatement(Filter filter){
        StringBuilder sb = new StringBuilder();
        if (filter != null) {
            if (filter.getName() != null) {
                sb.append(" WHERE m.name LIKE CONCAT ('%','").append(filter.getName()).append("','%') ");
            } else sb.append(" WHERE m.name LIKE '%' ");

            if (filter.getPlayTimeRange() != null) {
                sb.append(" AND m.playtime >= ").append(filter.getPlayTimeRange().getStart()).append(" AND m.playtime <= ").append(filter.getPlayTimeRange().getStart() + filter.getPlayTimeRange().getLength());
            }

            if (filter.getYearRange() != null) {
                sb.append(" AND m.year >= ").append(filter.getYearRange().getStart()).append(" AND m.year <= ").append(filter.getYearRange().getStart() + filter.getYearRange().getLength());
            }

            if ((filter.getCountries() != null) && (filter.getCountries().length != 0) && !(filter.getCountries()[0].equals(""))) {
                sb.append(" AND c.name IN ").append(Utils.toString(filter.getCountries(), "','", "('", "') "));
                sb.insert(0,"INNER JOIN countries AS c ON m.wikipedia_id = c.movies_wikipedia_id ");
            }

            if ((filter.getLanguages() != null) && (filter.getLanguages().length != 0) && !(filter.getLanguages()[0].equals(""))) {
                sb.append(" AND l.name IN ").append(Utils.toString(filter.getLanguages(), "','", "('", "') "));
                sb.insert(0,"INNER JOIN languages AS l ON m.wikipedia_id = l.movies_wikipedia_id ");
            }

            if ((filter.getGenres() != null) && (filter.getGenres().length != 0) && !(filter.getGenres()[0].equals(""))) {
                sb.append(" AND g.name IN ").append(Utils.toString(filter.getGenres(), "','", "('", "') "));
                sb.insert(0,"INNER JOIN genres AS g ON m.wikipedia_id = g.movies_wikipedia_id ");
            }
        }
        return sb.toString();
    }

    /**
     * Return the value of the number of movies matching the filter
     * @param filter
     * @return
     */
    public int countMovies(Filter filter){
        int result = 0;
        Connection connection = null;
        ResultSet resultSet;


        try {
            connection = getConnection();
            PreparedStatement pstm = null;
            StringBuilder sb = new StringBuilder();

            sb.append("SELECT COUNT(*) FROM (");
            sb.append(getSelectStatement());
            sb.append(getFilterStatement(filter));
            sb.append(") AS joined");

            System.out.println(sb.toString());

            pstm = connection.prepareStatement(sb.toString());
            resultSet = pstm.executeQuery();

            if(resultSet.next()){
                result = resultSet.getInt(1);
            }
            pstm.close();
            connection.close();
        } catch (Exception sqle) {
            sqle.printStackTrace();
        }

        return result;
    }

    /**
     *
     * @param c
     * @return
     */
    public Range getRange(ComparableAttribute c){
        Range result = null;
        int upper = 0;
        int lower = 0;
        switch(c){
            case PLAYTIME:
                lower = 0;
                upper = 500;
                result = new Range(lower,upper-lower);
                break;
            case YEAR:
                lower = 1850;
                upper = 2016;
                result = new Range(lower,upper-lower);
                break;
        }
        return result;
    }

    /**
     *
     * @param l
     * @return
     */
    public String[] getAll(ListAttribute l){
        ArrayList<String> result = new ArrayList<String>();
        Connection conn;
        ResultSet resultSet;
        String table="";

        try {
            PreparedStatement pstm = null;
            conn = getConnection();

            switch(l){
                case COUNTRIES:
                    table="countries";
                    break;
                case LANGUAGES:
                    table="languages";
                    break;
                case GENRES:
                    table="genres";
                    break;
            }
            pstm = conn.prepareStatement("SELECT DISTINCT name FROM "+table + " ORDER BY name DESC");
            resultSet = pstm.executeQuery();
            while(resultSet.next()){
                result.add(resultSet.getString("name"));
            }

            pstm.close();
            conn.close();
        } catch (Exception sqle) {
            sqle.printStackTrace();
        }
        return result.toArray(new String[result.size()]);
    }

    public HashMap<String,Integer> getGenresForCountry(String country, Filter filter){
        HashMap<String, Integer> result = new HashMap<>();
        Connection conn;
        ResultSet resultSet;

        try {
            PreparedStatement pstm = null;
            conn = getConnection();

            pstm = conn.prepareStatement("SELECT g.name FROM genres AS g INNER JOIN countries AS c on g.movies_wikipedia_id = c.movies_wikipedia_id WHERE c.name LIKE ? ");
            pstm.setString(1,country);
            resultSet = pstm.executeQuery();
            while(resultSet.next()) {
                String genre = resultSet.getString("g.name");
                if(result.containsKey(genre)){
                    result.put(genre,result.get(genre)+1);
                }else{
                    result.put(genre,1);
                }
            }

            pstm.close();
            conn.close();
        } catch (Exception sqle) {
            sqle.printStackTrace();
        }

        return result;
    }

    public HashMap<String,Integer> getMovieCountForAllCountries(Filter filter) {
        HashMap<String, Integer> result = new HashMap<>();
        String [] countries = getAll(ListAttribute.COUNTRIES);
        for (final String country : countries) {
            Filter tempFilter = filter;
            tempFilter.setCountries(Utils.toArray(country));
            int count = countMovies(tempFilter);
            if(count > 0) {
                result.put(country, count);
            }
        }
        return result;
    }
}