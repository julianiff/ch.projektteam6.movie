package ch.projektteam6.movie.server;

import java.io.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class FileUploadServlet extends HttpServlet{
    /**
     * Processes the file upload, initiated by the user
     * @param request The item which the user wants to upload
     * @param response The interface for the server to communicate with the client
     * @throws ServletException When something goes wrong with the servlet
     * @throws IOException When something goes wrong while trying to process data
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
        //contains the data from the user upload - provided by apache
        ServletFileUpload upload = new ServletFileUpload();

        try{
            //Get an iterator request to iterate through the files
            FileItemIterator iter = upload.getItemIterator(request);

            //We only allow one file at a time so it is sufficient
            //to check if the iterator has a item and then use that
            if (iter.hasNext()) {
                FileItemStream item = iter.next();
                //We get a writer to be able to update the client
                PrintWriter resp = response.getWriter();

                //If the file is not a .tsv, we do not parse it
                //We return an error to the client and exit the method
                if (!item.getContentType().equals("text/tab-separated-values")) {
                    resp.write("Upload failed!");
                    resp.write('\n');
                    resp.write("You have to submit a .tsv file!");
                    resp.flush();
                    resp.close();
                    return;
                }
                //gets the filename (i.e. movies_80000.tsv)
                String name = item.getFieldName();

                //We open the data stream to be able to
                InputStream stream = item.openStream();

                //create a BufferedReader and pass it to the importer to parse the file
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                Importer importer = new Importer();
                importer.readFile(reader);

                //If the upload, the parser and the databaseService all succeeded, we return
                //a message to the client and exit the method
                resp.write("Upload(" + item.getName() + ") successful!");
                resp.write('\n');
                resp.write(importer.status()[0]);
                resp.write('\n');
                resp.write("Parsed " + importer.status()[1] + " Movies");
                resp.flush();
                resp.close();
            }
        }
        catch(Exception e){
            throw new RuntimeException(e);
        }

    }
}