package ch.projektteam6.movie.server;

import ch.projektteam6.movie.shared.Movie;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MovieParser {
    /**
     * Parses a String which represents a Movie, whose elements are separated by tabs.
     * @pre There has to be a tab (\t) for every movie element (even for the missing ones)
     * @param line The String which contains the movie elements
     * @return returns a Movie object which contains the parsed elements
     */
    public static Movie parseMovie(String line) {
        //Splits the string around tabs and save each part as a new array element
        //these is the reason why there has to be a tab for missing elements
        String[] cleanedString = line.split("\\t");
        Movie parsedMovie = new Movie();

        //loop through the movie elements
        //we have to do some cleanup before we save them in the corresponding Movie class
        for (int i = 0; i < cleanedString.length; i++) {
            //if an element is missing (i.e. no running time) we go to the next element
            //the missing one gets the default values (String -> null, int -> 0)
            //if the wikipedia ID is missing, we don't parse the movie
            if (cleanedString[i].isEmpty()) {
                if (i == 0) {
                    return null;
                }
                continue;
            }
            switch (i) {
                case 0: parsedMovie.setWikipediaID(Integer.parseInt(cleanedString[i]));
                    break;
                //case 1 is the freebase ID which we don't care about
                case 2: parsedMovie.setName(cleanedString[i]);
                    break;
                case 3: parsedMovie.setYear(MovieParser.parseDate(cleanedString[i]));
                    break;
                //case 4 is the box office revenue which we also don't care about
                case 5: parsedMovie.setPlayTime(Math.round(Float.parseFloat(cleanedString[i])));
                    break;
                case 6: parsedMovie.setLanguages(parseFreebaseItem(cleanedString[i]));
                    break;
                case 7: parsedMovie.setCountries(parseFreebaseItem(cleanedString[i]));
                    break;
                case 8: parsedMovie.setGenres(parseFreebaseItem((cleanedString[i])));
                    break;
            }
        }
        return parsedMovie;
    }

    /**
     * Parses a Freebase item and cleans it up
     * @pre item is in form {"ID": "Value"}
     * @param item String which represents a freebase item
     * @return returns an array which contains the values of a freebase item
     */
    public static String[] parseFreebaseItem(String item) {
        //Splits the string around commas and save each part as an array element
        //if there is no ', ', the whole string is saved as the first array element
        String[] items = item.split(", ");
        String[] parsedItems = new String[items.length];
        for (int i = 0; i < items.length; i++) {
            //Some regex to get the value
            //wants a ", followed by at least one letter, followed by an optional space
            //and some optional items like '-'. This pattern must appear at least one time
            //stops by the second '"'
            Pattern p = Pattern.compile("\"(\\w+\\s?[-'/]?)+");
            Matcher m = p.matcher(items[i]);
            if (m.find()) {
                //saves the value, beginning at index 1, since our regex pattern includes the first '"'
                parsedItems[i] = m.group().substring(1);
            }
        }
        return parsedItems;
    }

    /**
     * Parses a String which represents a date and cleans it up
     * @pre date is in one of the following formats: YYYY / YYYY-MM / YYYY-MM-DD
     * @param date String which represents a date
     * @return returns an integer which contains only the year
     */
    public static int parseDate(String date) {
        Pattern wholeDate = Pattern.compile("^(\\d{4}-\\d{2}(-\\d{2})?)$|^(\\d{4})$");
        Matcher checkIfDate = wholeDate.matcher(date);

        //if we find a date in the String...
        if (checkIfDate.find()) {
            //... we parse the year and return it as integer
            Pattern year = Pattern.compile("^\\d{4}");
            Matcher m = year.matcher(date);
            if (m.find()) {
                return Integer.parseInt(m.group());
            }
        }
        //otherwise we return -1, since then, something bad happened before
        return -1;
    }

}
