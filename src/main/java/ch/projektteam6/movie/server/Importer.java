package ch.projektteam6.movie.server;

import ch.projektteam6.movie.shared.Movie;

import java.io.*;
import java.util.ArrayList;

public class Importer {

    private ArrayList<Movie> movies = new ArrayList<Movie>();
    private String state = "";
    private DataBaseServiceImpl dbService = new DataBaseServiceImpl();

    /**
     * reads a file, which contains movies, line by line
     * @pre Input stream (which the reader is initialized from) is a .tsv with one movie per line
     * @param reader BufferedReader which contains the input stream from a .tsv file
     */
    public void readFile(BufferedReader reader) {
        try {
            String currentLine = reader.readLine();
            while (currentLine != null) {
                //let the movie parser parse the current line and add the resulting Movie object to movies
                movies.add(MovieParser.parseMovie(currentLine));
                currentLine = reader.readLine();
            }
            state = "Parsing successful!";
            reader.close();
            //write the movies inside the datastore
            dbService.writeMovies(movies);

        } catch (IOException ex) {
            System.out.println("Error while reading file!");
            state = "Error while parsing!";
            ex.printStackTrace();
        }
    }

    /**
     * Returns a status message (needed for the file upload)
     * @return returns an array which contains the status as well as the amount of parsed movies
     */
    public String[] status() {
        String status[] = new String[2];
        status[0] = state;
        status[1] = Integer.toString(movies.size());
        return status;
    }
}