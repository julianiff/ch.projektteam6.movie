package ch.projektteam6.movie.client;

import ch.projektteam6.movie.shared.Movie;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by lukasvollenweider on 09/11/15.
 */
public class DummieTest {
    /********************************************************/
    /*    public static ArrayList<Movie> create(int, int)   */
    /********************************************************/
    @Test
    public void testFunctionWhichCreatesDummieMoviesWithValidInput() {
        ArrayList<Movie> movies = Dummie.create(0, 5);

        assertThat(movies.size(), equalTo(5));
    }

    @Test
    public void testFunctionWhichCreatesDummieMoviesWithInvalidInput() {
        ArrayList<Movie> movies = Dummie.create(5,0);

        assertThat(movies.size(), equalTo(0));
    }

}