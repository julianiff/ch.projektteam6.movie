package ch.projektteam6.movie.shared;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.gwtbootstrap3.extras.slider.client.ui.Range;

public class UtilsTest {

    /*******************************************************************/
    /* public static String toString(String[], String, String, String) */
    /*******************************************************************/
    @Test
    public void testArrayToStringWithPreAndPostElements() {
        String[] testInput = {"Hallo", "ich", "bin", "ein", "Test", "!"};
        String pre = "TestBegin ";
        String post = " TestEnd";

        String expectedOutput = "TestBegin Hallo ich bin ein Test ! TestEnd";

        String result = Utils.toString(testInput, " ", pre, post);

        assertThat(result, equalTo(expectedOutput));
    }

    @Test
    public void testArrayToStringWithPreNullAndPostNull() {
        String[] testInput = {"Hallo", "ich", "bin", "ein", "Test", "!"};

        String expectedOutput = "nullHallo ich bin ein Test !null";

        String result = Utils.toString(testInput, " ", null, null);

        assertThat(result, equalTo(expectedOutput));
    }

    /*******************************************************************/
    /*          public static String toString(String[], String)        */
    /*******************************************************************/
    @Test
    public void testArrayToStringConverterWithSeparator() {
        String[] testInput = {"Hallo", "ich", "bin", "ein", "Test", "!"};
        String separator = " ";

        String expectedOutput = "Hallo ich bin ein Test !";

        String result = Utils.toString(testInput, separator);

        assertThat(result, equalTo(expectedOutput));
    }

    /*******************************************************************/
    /*             public static String toString(String[])             */
    /*******************************************************************/
    @Test
    public void  testArrayToStringConverterWithDefaultSeparator() {
        String[] testInput = {"Hallo", "ich", "bin", "ein", "Test", "!"};

        String expectedOutput = "Hallo, ich, bin, ein, Test, !";

        String result = Utils.toString(testInput);
        assertThat(result, equalTo(expectedOutput));
    }

    @Test
    public void testArrayToStringWithEmptyArrayAndDefaultSeparator() {
        String[] testInput = {};

        String expectedOutput = "";

        String result = Utils.toString(testInput);
        assertThat(result, equalTo(expectedOutput));
    }

    @Test
    public void testArrayToStringWithArrayContainingOnlyEmptyStringsAndDefaultSeparator() {
        String[] testInput = {"", "", "", "", ""};

        String expectedOutput = ", , , , ";

        String result = Utils.toString(testInput);
        assertThat(result, equalTo(expectedOutput));
    }

    @Test
    public void testArrayToStringWithNull() {
        try {
            String exptectedOutput = null;
            String result = Utils.toString(null);

            assertThat(result, equalTo(exptectedOutput));

            fail();
        } catch (NullPointerException ex) {}
    }


    /*******************************************************************/
    /*          public static String[] toArray(String, String)         */
    /*******************************************************************/
    @Test
    public void testStringToArrayWithSeparator() {
        String testInput = "Hallo, ich bin ein Test!";
        String[] expectedResult = new String[]{"Hallo,", "ich", "bin", "ein", "Test!"};
        String[] result = Utils.toArray(testInput, " ");

        assertThat(result, equalTo(expectedResult));
    }

    /*******************************************************************/
    /*              public static String[] toArray(String)             */
    /*******************************************************************/
    @Test
    public void testStringToArrayWithDefaultSeparator() {
        String testInput = "Hallo, ich bin ein Test!";
        String[] expectedResult = new String[]{"Hallo", "ich bin ein Test!"};
        String[] result = Utils.toArray(testInput);

        assertThat(result, equalTo(expectedResult));
    }

    @Test
    public void testStringToArrayWithEmptyString() {
        String testInput = "";
        String[] expectedResult = new String[]{""};
        String[] result = Utils.toArray(testInput);

        assertThat(result, equalTo(expectedResult));
    }

    @Test
    public void testStringToArrayWithNull() {
        try {
            String[] expectedResult = new String[]{""};
            String[] result = Utils.toArray(null);

            assertThat(result, equalTo(expectedResult));

            fail();
        } catch (NullPointerException ex) {}

    }

    /**************************************************************************/
    /* public static Range toBootstrapRange(com.google.gwt.view.client.Range) */
    /**************************************************************************/
    @Test
    public void testRangeToBootstrapRangeWithNull() {
        try {
            Range expectedResult = null;
            Range result = Utils.toBootstrapRange(null);

            assertThat(result, equalTo(expectedResult));

            fail();
        } catch (NullPointerException ex) {}
    }

    /**************************************************************************/
    /*   public static com.google.gwt.view.client.Range toGWTRange(Range r)   */
    /**************************************************************************/
    @Test
    public void testBootstrapToGWTRange() {
        Range testInput = new Range(0.3, 5.9);
        com.google.gwt.view.client.Range expectedResult = new com.google.gwt.view.client.Range(0,5);

        com.google.gwt.view.client.Range result = Utils.toGWTRange(testInput);

        assertThat(result, equalTo(expectedResult));
    }

    @Test
    public void testBootstrapToGWTRangeWithNull() {
        try {
            com.google.gwt.view.client.Range result = Utils.toGWTRange(null);
            fail();
            assertThat(result, equalTo(null));
        } catch (NullPointerException ex) {}
    }
}