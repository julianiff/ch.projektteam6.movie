package ch.projektteam6.movie.server;

import ch.projektteam6.movie.shared.Movie;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.destination.DriverManagerDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.hamcrest.CoreMatchers.*;

import static org.junit.Assert.*;

public class ImporterTest {
    /*******************************************************/
    /*     public void readFile(BufferedReader reader)     */
    /*******************************************************/
    @Test
    public void testFileReaderWithTestFile() {
        String filePath = "src/test/test-database.tsv";
        String[] expectedStatus = {"Parsing successful!", "165"};
        Importer importer = new Importer();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            importer.readFile(reader);
            assertThat(importer.status(), equalTo(expectedStatus));
        } catch (FileNotFoundException ex) {
            fail();
            ex.printStackTrace();
        }
    }

    @Test
    public void testFileReaderWithInvalidFile() {
        String filePath = "src/test/not_a_valid_file";
        Importer importer = new Importer();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            fail();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    /*******************************************************/
    /*              public String[] status()               */
    /*******************************************************/
    // Not really necessary to write JUnits for this little
    // method since nothing can go wrong here

}