package ch.projektteam6.movie.server;

import ch.projektteam6.movie.shared.Movie;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.assertj.core.api.Assertions.*;

import static org.junit.Assert.*;

public class MovieParserTest {
    /*******************************************************/
    /*       public static Movie parseMovie(String)        */
    /*******************************************************/
    @Test
    public void testMovieParserWithCompleteMovieAsInput() {
        String movie = "3196793\t/m/08yl5d\tGetting Away with Murder: The JonBenét Ramsey Mystery\t2000-02-16\t\t95.0\t{\"/m/02h40lc\": \"English Language\"}\t{\"/m/09c7w0\": \"United States of America\"}\t{\"/m/02n4kr\": \"Mystery\", \"/m/03bxz7\": \"Biographical film\", \"/m/07s9rl0\": \"Drama\", \"/m/0hj3n01\": \"Crime Drama\"}";
        Movie expectedResult = new Movie("Getting Away with Murder: The JonBenét Ramsey Mystery", 2000, new String[] {"United States of America"}, new String[]{"Mystery", "Biographical film", "Drama", "Crime Drama"}, new String[] {"English Language"}, 95, 3196793);

        Movie result = MovieParser.parseMovie(movie);

        assertThat(result).isEqualToComparingFieldByField(expectedResult);
    }

    @Test
    public void testMovieParserWithMovieWhichMissesSomeElementsAsInput() {
        String movie = "3196793\t/m/08yl5d\t\t2000-02-16\t\t95.0\t{\"/m/02h40lc\": \"English Language\"}\t\t{\"/m/02n4kr\": \"Mystery\", \"/m/03bxz7\": \"Biographical film\", \"/m/07s9rl0\": \"Drama\", \"/m/0hj3n01\": \"Crime Drama\"}";
        Movie expectedResult = new Movie(null, 2000, null, new String[]{"Mystery", "Biographical film", "Drama", "Crime Drama"}, new String[] {"English Language"}, 95, 3196793);

        Movie result = MovieParser.parseMovie(movie);

        assertThat(result).isEqualToComparingFieldByField(expectedResult);
    }

    @Test
    public void testMovieParserWithMissingWikiIDAsInput() {
        String movie = "\t/m/08yl5d\tGetting Away with Murder: The JonBenét Ramsey Mystery\t2000-02-16\t\t95.0\t{\"/m/02h40lc\": \"English Language\"}\t{\"/m/09c7w0\": \"United States of America\"}\t{\"/m/02n4kr\": \"Mystery\", \"/m/03bxz7\": \"Biographical film\", \"/m/07s9rl0\": \"Drama\", \"/m/0hj3n01\": \"Crime Drama\"}";
        Movie expectedResult = null;

        Movie result = MovieParser.parseMovie(movie);

        assertThat(result, equalTo(expectedResult));
    }

    @Test
    public void testMovieParserWithEmptyMoviesAsInput() {
        String movie = "\t\t\t\t\t\t\t\t";
        Movie expectedResult = new Movie(null, 0, null, null, null, 0, 0);

        Movie result = MovieParser.parseMovie(movie);

        assertThat(result).isEqualToComparingFieldByField(expectedResult);
    }

    @Test
    public void testMovieParserWithEmptyLineAsInput() {
        String movie = "\t";
        Movie expectedResult = new Movie(null, 0, null, null, null, 0, 0);

        Movie result = MovieParser.parseMovie(movie);

        assertThat(result).isEqualToComparingFieldByField(expectedResult);
    }

    @Test
    public void testMovieParserWithNullAsInput() {
        try {
            Movie result = MovieParser.parseMovie(null);

            assertThat(result, equalTo(null));

            fail();
        } catch(NullPointerException ex) {}

    }

    /*******************************************************/
    /*   public static String[] parseFreebaseItem(String)  */
    /*******************************************************/
    @Test
    public void testFreebaseItemParserWithOneItemAsInput() {
        String item = "{\"/m/0lsxr\": \"Crime Fiction\", \"/m/07s9rl0\": \"Drama\"}";

        String[] result = MovieParser.parseFreebaseItem(item);

        assertThat(result, equalTo(new String[] {"Crime Fiction", "Drama"}));
    }

    @Test
    public void testFreebaseItemParserWithMultipleItemsAsInput() {
        String item = "{\"/m/02h40lc\": \"English Language\"}";

        String[] result = MovieParser.parseFreebaseItem(item);

        assertThat(result, equalTo(new String[] {"English Language"}));
    }


    @Test
    public void testFreebaseItemParserWithNoItemAsInput() {
        String item = "NotAnItem";

        String[] result = MovieParser.parseFreebaseItem(item);

        assertThat(result, equalTo(new String[] {null}));
    }

    @Test
    public void testFreebaseItemParserWithNullAsInput() {
        try {
            String[] result = MovieParser.parseFreebaseItem(null);
            fail();
        } catch(NullPointerException ex){}
    }

    /*******************************************************/
    /*         public static int parseDate(String)         */
    /*******************************************************/
    @Test
    public void testDateParserWithInputFormatYYYY() {
        String date = "1900";

        int result = MovieParser.parseDate(date);

        assertThat(result, equalTo(1900));
    }

    @Test
    public void testDateParserWithInputFormatYYYYMM() {
        String date = "1900-12";

        int result = MovieParser.parseDate(date);

        assertThat(result, equalTo(1900));
    }

    @Test
    public void testDateParserWithInputFormatYYYYMMDD() {
        String date = "1900-12-31";

        int result = MovieParser.parseDate(date);

        assertThat(result, equalTo(1900));
    }

    @Test
    public void testDateParserWithWrongInput() {
        String date = "NotADate";
        int result = MovieParser.parseDate(date);

        assertThat(result, equalTo(-1));
    }

    @Test
    public void testDateParserWithNullAsInput() {
        try {
            int result = MovieParser.parseDate(null);
            fail();
        } catch(NullPointerException ex){}
    }
}