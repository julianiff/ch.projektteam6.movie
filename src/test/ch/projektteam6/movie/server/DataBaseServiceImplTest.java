package ch.projektteam6.movie.server;

import ch.projektteam6.movie.shared.ComparableAttribute;
import ch.projektteam6.movie.shared.Filter;
import ch.projektteam6.movie.shared.Movie;
import com.google.gwt.view.client.Range;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.destination.DriverManagerDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import static com.ninja_squad.dbsetup.Operations.*;

import mockit.Mock;
import mockit.MockUp;

import java.sql.Connection;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.assertj.core.api.Assertions.*;

import static org.junit.Assert.*;




public class DataBaseServiceImplTest {

    private static String username = "root";
    private static String password = "test12";
    private static String URL = "jdbc:mysql://localhost:3306/unittesting";

    private DriverManagerDestination driverManager;

    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();

    @Before
    public void prepare() throws Exception {
        Operation operation =
                sequenceOf(
                        DatabaseHelper.DELETE_ALL,
                        DatabaseHelper.INSERT_REFERENCE_DATA);

        driverManager = new DriverManagerDestination(URL, username, password);

        DbSetup dbSetup = new DbSetup(driverManager, operation);

        dbSetupTracker.launchIfNecessary(dbSetup);
    }

    /********************************************************/
    /*      public void writeMovies(ArrayList<Movie>)       */
    /********************************************************/
    @Test
    public void testWriteMoviesToDatabase() {
        //overwrite the function getConnection from the DataBaseServiceImpl class
        //to return the connection from the local testdatabase
        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        Movie movie = new Movie("test", 1223, new String[]{"test"}, new String[]{"test"}, new String[]{"test"}, 200, 91384);

        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(movie);
        dbService.writeMovies(movies);

        ArrayList<Movie> result = dbService.readMovies(new Filter());
        assertThat(result.size(), equalTo(4));
    }

    @Test
    public void testIfWriteWritesMovieCorrectIntoDatabase() {
        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        Movie movie = new Movie("test", 1223, new String[]{"test"}, new String[]{"test"}, new String[]{"test"}, 200, 91384);

        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(movie);
        dbService.writeMovies(movies);

        ArrayList<Movie> result = dbService.readMovies(new Filter());

        int i = 0;
        while (i < result.size()) {
            if (result.get(i).getWikipediaID() == movie.getWikipediaID()) {
                break;
            }
            i++;
        }

        assertThat(result.get(i)).isEqualToComparingFieldByField(movie);
    }

    @Test
    public void testWriteMoviesToDatabaseWithEmptyMovie() {
        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        ArrayList<Movie> movies = new ArrayList<>();
        Movie movie = new Movie();
        movies.add(movie);

        dbService.writeMovies(movies);

        ArrayList<Movie> result = dbService.readMovies(new Filter());
        assertThat(result.size(), equalTo(3));
    }

    @Test
    public void testWriteMoviesToDatabaseWithNULL() {
        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        dbService.writeMovies(null);

        ArrayList<Movie> result = dbService.readMovies(new Filter());
        assertThat(result.size(), equalTo(3));
    }

    /********************************************************/
    /*      public ArrayList<Movie> readMovies(Filter)      */
    /********************************************************/
    @Test
    public void testReadMoviesFromDatabaseWithEmptyFilter() {
        //only create dbSetub if necessary to optimize unit test speed
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        ArrayList<Movie> result = dbService.readMovies(new Filter());
        assertThat(result.size(), equalTo(3));
    }

    @Test
    public void testReadMoviesFromDatabaseWithCountryFilter() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        Filter filter = new Filter();
        filter.setCountries(new String[]{"United States of America"});
        ArrayList<Movie> movies = dbService.readMovies(filter);
        assertThat(movies.size(), equalTo(2));
    }

    @Test
    public void testReadMoviesFromDatabaseWithLanguageFilter() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        Filter filter = new Filter();
        filter.setLanguages(new String[]{"Norwegian Language"});
        ArrayList<Movie> movies = dbService.readMovies(filter);
        assertThat(movies.size(), equalTo(1));
    }

    @Test
    public void testReadMoviesFromDatabaseWithNameFilter() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        Filter filter = new Filter();
        filter.setName("Birth");
        ArrayList<Movie> movies = dbService.readMovies(filter);
        assertThat(movies.size(), equalTo(1));
    }

    @Test
    public void testReadMoviesFromDatabaseWithGenreFilter() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        Filter filter = new Filter();
        filter.setGenres(new String[]{"Thriller"});
        ArrayList<Movie> movies = dbService.readMovies(filter);
        assertThat(movies.size(), equalTo(2));
    }

    @Test
    public void testReadMoviesFromDatabaseWithPlaytimeFilter() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        Filter filter = new Filter();
        filter.setPlayTimeRange(new Range(80,200));
        ArrayList<Movie> movies = dbService.readMovies(filter);
        assertThat(movies.size(), equalTo(3));
    }

    @Test
    public void testReadMoviesFromDatabaseWithMultipleElementsAsFilter() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        Filter filter = new Filter();
        filter.setLanguages(new String[]{"Norwegian Language"});
        filter.setPlayTimeRange(new Range(90,200));
        ArrayList<Movie> movies = dbService.readMovies(filter);
        assertThat(movies.size(), equalTo(1));
    }

    /*************************************************************************/
    /*        public ArrayList<Movie> readMovies(Filter, int, int)           */
    /*************************************************************************/
    @Test
    public void testReadingMoviesInsideARange() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        ArrayList<Movie> movies = dbService.readMovies(new Filter(), 0, 1);
        assertThat(movies.size(), equalTo(1));
    }

    @Test
    public void testReadingMoviesWithInvalidRange() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        ArrayList<Movie> movies = dbService.readMovies(new Filter(), 0, 0);
        assertThat(movies.size(), equalTo(0));
    }

    /*************************************************************************/
    /* public ArrayList<Movie> readMovies(Filter, int, int, String, boolean) */
    /*************************************************************************/
    @Test
    public void testSortingMoviesWithInvalidArgument() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        ArrayList<Movie> movies = dbService.readMovies(new Filter(), 0, 3, "languages", false);
        assertThat(movies.size(), equalTo(0));
    }

    @Test
    public void testSortingMoviesWithValidArgumentDescending() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        ArrayList<Movie> movies = dbService.readMovies(new Filter(), 0, 3, "playtime", false);
        assertThat(movies.get(0).getName(), equalTo("The Birth of a Nation"));
    }

    @Test
    public void testSortingMoviesWithValidArgumentAscending() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        ArrayList<Movie> movies = dbService.readMovies(new Filter(), 0, 3, "year", true);
        assertThat(movies.get(0).getName(), equalTo("The Birth of a Nation"));
    }

    /*************************************************************************/
    /*                     public int countMovies(Filter)                    */
    /*************************************************************************/
    @Test
    public void testCountMoviesWithEmptyFilter() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        int count = dbService.countMovies(new Filter());

        assertThat(count, equalTo(3));
    }

    @Test
    public void testCountMoviesWithFilter() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        Filter filter = new Filter();
        filter.setLanguages(new String[]{"English Language"});

        int count = dbService.countMovies(filter);

        assertThat(count, equalTo(2));
    }

    /*************************************************************************/
    /*             public Range getRange(ComparableAttribute)                */
    /*************************************************************************/
    @Test
    public void testGetRangeWithInvalidArgument() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();
        try {
            Range result = dbService.getRange(null);
            fail();
        } catch(NullPointerException ex){}
    }

    @Test
    public void testGetRangeWithValidArgument() {
        dbSetupTracker.skipNextLaunch();

        new MockUp<DataBaseServiceImpl>() {
            @Mock
            Connection getConnection() throws Exception {
                return driverManager.getConnection();
            }
        };

        DataBaseServiceImpl dbService = new DataBaseServiceImpl();

        Range result = dbService.getRange(ComparableAttribute.PLAYTIME);

        assertThat(result, equalTo(new Range(0,500)));
    }
}
