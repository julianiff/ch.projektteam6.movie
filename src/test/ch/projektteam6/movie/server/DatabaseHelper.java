package ch.projektteam6.movie.server;
import com.ninja_squad.dbsetup.operation.Operation;

import static com.ninja_squad.dbsetup.Operations.*;


public class DatabaseHelper {

    public static final Operation DELETE_ALL = deleteAllFrom("languages", "genres", "countries", "movies");

    public static final Operation INSERT_REFERENCE_DATA =
            sequenceOf(
                    insertInto("languages")
                        .columns("id", "name", "movies_wikipedia_id")
                        .values(1, "English Language", 975900)
                        .values(2, "English Language", 3196793)
                        .values(3, "Norwegian Language", 28463795)
                        .build(),
                    insertInto("genres")
                        .columns("id", "name", "movies_wikipedia_id")
                        .values(1, "Thriller", 975900)
                        .values(2, "Science Fiction", 975900)
                        .values(3, "Horror", 975900)
                        .values(4, "Mystery", 3196793)
                        .values(5, "Thriller", 28463795)
                        .build(),
                    insertInto("countries")
                        .columns("id", "name", "movies_wikipedia_id")
                        .values(1, "United States of America", 975900)
                        .values(2, "United States of America", 3196793)
                        .values(3, "Norway", 28463795)
                        .build(),
                    insertInto("movies")
                        .columns("name", "year", "wikipedia_id", "playtime")
                        .values("Actrius", 1994, 975900, 90)
                        .values("Army of Darkness", 1992, 3196793, 81)
                        .values("The Birth of a Nation", 1915, 28463795, 190)
                        .build());
}
